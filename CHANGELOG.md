# Change Log
All notable changes to this project will be documented in this file.

## 0.3.4 - 2016-05-25
### Added
- Automatic install of NTP.
- Newrelic role (RHEL only).
- production_base.plbk to install all base software used on TACC-hosted production servers.

### Changed
- Bundle Ansible source directly in image since relying on specific tags from the repository broke things over time.
- Update to the splunk role to use a specific forwarder archive bundled with deployer and to increase idempotence.
- Fixes to iReceptor hap configs.
- Updates to the Adama API definition to use a new host in ec2 and turn off API suspensions.
- Fixes to public.agaveapi.co host and httpd server configs to prefer the public.agaveapi.co domain (changes had not been ported
from docker_atim)

### Removed
- Docker version detection code (agave_auth role) which was no longer in use and breaking some playbooks when running from Jenkins.


## 0.3.3 - 2016-05-20
### Added
- No change.

### Changed
- Turned off API suspension behavior with correct configs for all core APIs.

### Removed
- No change.


## 0.3.2 - 2016-05-19
### Added
- Parameterized the agave_core_sql_migrations role with core_docker_registry_account and core_docker_private_registry
variables.
- Added core_migrations.plbk playbook

### Changed
- Integrated the core sql migration role into existing playbooks.

### Removed
- No change.


## 0.3.1 - 2016-05-18
### Added
- Added optional configurations core_docker_registry_account and core_docker_private_registry for retrieving core API
containers, enabling development builds to be pulled from a private registry or dev account.

### Changed
- No change

### Removed
- No change.


## 0.3.0 - 2016-05-18
### Added
- Core services LB Proxy is now an optional deployment and not deployed by default.
- Several new optional configurations for the core science APIs are available; e.g. mem limits, paging configs, relay transfer.
- Added configs to all core APIs to turn off API suspension behavior.
- Added the Agave branded OAuth app.
- Added the AIP branded OAuth app.
- Expose option to launch Jupyterhub user containers in privileged mode and other Docker host config options.

### Changed
- Core service deployment updates to accommodate 2.1.8 release.
- Added HAP configs for remaining production auth tenants.
- Fixed bug where the default oauth app could get copied to the host unnecessarily.
- Updates to the Designsafe branded OAuth app for responsiveness.
- Update to the directories mounted in the Designsafe hub user containers.

### Removed
- No change.


## 0.2.0 - 2016-04-29
### Added
- Added support for A/B deployments of auth services and a rolling update playbook for (near) zero downtime deployments.
- Added a custom Agave-branded OAuth login application that ships, be default, for each tenants.
- Added a playbook to update Docker installation and auth services simultaneously.
- Added support for A/B deployment of core services.
- Added a role to support sql migrations for the core services.
- Added config files for centos7 sandbox deployment.
- Added support for multiple Agave Jupyterhub instances. Deployment and updates can now done from a configuration directory/
- Added a Jupyterhub update playbook.
- Added support for runtime configuration of all aspects of the Jupyterhub (had previously been baked into the image).
- Added support for custom login page for Jupyterhub.

### Changed
- Disabled chunk transfer encoding from Files, Jobs, Postits and Transforms.
- Changes to the DS custom OAuth login application.
- Added httpbin cert support to APIM.
- Numerous fixes to the core deployment to keep pace with the latest developments.
- Docker role and playbook now deploys precise versions of the tools.
- Moved core and auth mysql container to the official mysql image.
- Updated Jupyterhub projects to build from the latest Jupyterhub releases.
- Jupyterhub images are now under the taccsciapps docker hub organization.
- Fixed wildcard cert issue an vanity DNS preference issue in AIP configs.

### Removed
- Removed various linking behavior in the auth compose files.


## 0.1.1 - 2016-03-29
### Added
- Added support for deploying swagger definition files from auth apache container.

### Changed
- No change.

### Removed
- No change.


## 0.1.0 - 2016-03-19
### Added
- Project CHANGELOG.md file.
- Added support for deploying core APIs using A/B deployment.

### Changed
- Deploying core services now requires a configuration file similar to the tenant config.yml file. See docs for required fields.

### Removed
- No change.