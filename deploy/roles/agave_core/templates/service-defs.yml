###############################################################
# Core Science APIs - Staging
# docker-compose.yml
#
# This is the orchestration file to start Agave's Core Science
# APIs. The APIs are accessible behind a proxy container which
# shields them from the outside world. Containers are assigned
# static ports to enable rolling updates of each API as needed.
# Containers performing data operations are assigned a partition
# of the default GridFTP and FTP port ranges to enable proper
# operation of the protocol from within each container.
#
# Make sure to start your containers
# with $AGAVE_VERSION defined in your environment. You should
# also have the following /etc/hosts entries defined for your
# hosting environment. (These are appropriate for docker-machine
# on OSX.) In a production environment, these should resolve
# to the appropriate hosts and/or dns entries.
#
#
###############################################################
#   Agave Core Services
###############################################################

jobs:
  extends:
    file: common.yml
    service: java_api
  image: {{ core_docker_registry_account }}/jobs-api:$AGAVE_VERSION
  mem_limit: 2048m
  environment:
    - 'IPLANT_MAX_STAGING_TASKS=5'
    - 'IPLANT_MAX_ARCHIVE_TASKS=5'
    - 'IPLANT_MAX_SUBMISSION_TASKS=2'
    - 'IPLANT_MAX_MONITORING_TASKS=1'
    - 'GLOBUS_TCP_PORT_RANGE=50007,50017'
    - 'CATALINA_OPTS="-Duser.timezone=America/Chicago -Djsse.enableCBCProtection=false -Djava.awt.headless=true -Dfile.encoding=UTF-8 -server -Xms512m -Xmx2048m -XX:+DisableExplicitGC -Djava.security.egd=file:/dev/./urandom"'
  volumes:
    - '/var/log/splunk/jobs:/opt/tomcat/logs'
    - './scratch/jobs:/scratch'

files:
  extends:
    file: common.yml
    service: java_api
  image: {{ core_docker_registry_account }}/files-api:$AGAVE_VERSION
  mem_limit: 2048m
  environment:
    - 'IPLANT_MAX_STAGING_TASKS=5'
    - 'IPLANT_MAX_TRANSFORM_TASKS=5'
    - 'GLOBUS_TCP_PORT_RANGE=50301,50310'
    - 'CATALINA_OPTS="-Duser.timezone=America/Chicago -Djsse.enableCBCProtection=false -Djava.awt.headless=true -Dfile.encoding=UTF-8 -server -Xms512m -Xmx2048m -XX:+DisableExplicitGC -Djava.security.egd=file:/dev/./urandom"'
  volumes:
    - '/var/log/splunk/files:/opt/tomcat/logs'
    - './scratch/files:/scratch'

apps:
  extends:
    file: common.yml
    service: java_api
  image: {{ core_docker_registry_account }}/apps-api:$AGAVE_VERSION
  hostname: api.staging.agaveapi.co
  environment:
    - 'GLOBUS_TCP_PORT_RANGE=50901,50910'
    - 'CATALINA_OPTS="-Duser.timezone=America/Chicago -Djsse.enableCBCProtection=false -Djava.awt.headless=true -Dfile.encoding=UTF-8 -server -Xms256m -Xmx1024m -XX:+DisableExplicitGC -Djava.security.egd=file:/dev/./urandom"'
  volumes:
    - '/var/log/splunk/apps:/opt/tomcat/logs'
    - './scratch/apps:/scratch'

systems:
  extends:
    file: common.yml
    service: java_api
  image: {{ core_docker_registry_account }}/systems-api:$AGAVE_VERSION
  mem_limit: 512m
  environment:
    - 'CATALINA_OPTS="-Duser.timezone=America/Chicago -Djsse.enableCBCProtection=false -Djava.awt.headless=true -Dfile.encoding=UTF-8 -server -Xms256m -Xmx512m -XX:+DisableExplicitGC -Djava.security.egd=file:/dev/./urandom"'
  volumes:
    - '/var/log/splunk/systems:/opt/tomcat/logs'
    - './scratch/systems:/scratch'

monitors:
  extends:
    file: common.yml
    service: java_api
  image: {{ core_docker_registry_account }}/monitors-api:$AGAVE_VERSION
  hostname: api.staging.agaveapi.co
  mem_limit: 1024m
  restart: "always"
  volumes:
    - '/var/log/splunk/monitors:/opt/tomcat/logs'
    - './scratch/monitors:/scratch'

profiles:
  extends:
    file: common.yml
    service: java_api
  image: {{ core_docker_registry_account }}/profiles-api:$AGAVE_VERSION
  mem_limit: 512m
  environment:
    - 'CATALINA_OPTS="-Duser.timezone=America/Chicago -Djsse.enableCBCProtection=false -Djava.awt.headless=true -Dfile.encoding=UTF-8 -server -Xms256m -Xmx512m -XX:+DisableExplicitGC -Djava.security.egd=file:/dev/./urandom"'
  volumes:
    - '/var/log/splunk/profiles:/opt/tomcat/logs'
    - './scratch/profiles:/scratch'

metadata:
  extends:
    file: common.yml
    service: java_api
  image: {{ core_docker_registry_account }}/metadata-api:$AGAVE_VERSION
  volumes:
    - '/var/log/splunk/metadata:/opt/tomcat/logs'
    - './scratch/metadata:/scratch'


notifications:
  extends:
    file: common.yml
    service: java_api
  image: {{ core_docker_registry_account }}/notifications-api:$AGAVE_VERSION
  environment:
    - 'IPLANT_MAX_NOTIFICATION_TASKS=2'
  volumes:
    - '/var/log/splunk/notifications:/opt/tomcat/logs'
    - './scratch/notifications:/scratch'

transforms:
  extends:
    file: common.yml
    service: java_api
  image: {{ core_docker_registry_account }}/transforms-api:$AGAVE_VERSION
  environment:
    - 'IPLANT_MAX_STAGING_TASKS=2'
    - 'IPLANT_MAX_ARCHIVE_TASKS=2'
    - 'IPLANT_MAX_SUBMISSION_TASKS=2'
    - 'IPLANT_MAX_TRANSFORM_TASKS=2'
    - 'GLOBUS_TCP_PORT_RANGE=50601,50610'
  volumes:
    - '/var/log/splunk/transforms:/opt/tomcat/logs'
    - './scratch/transforms:/scratch'

postits:
  extends:
    file: common.yml
    service: php_api
  image: {{ core_docker_registry_account }}/postits-api:$AGAVE_VERSION
  environment:
    - 'IPLANT_PROXY_SERVICE={{ agave_core_iplant_proxy_service }}'
  volumes:
    - '/var/log/splunk/postits:/var/log/apache2'
    - './scratch/postits:/scratch'

usage:
  extends:
    file: common.yml
    service: php_api
  image: {{ core_docker_registry_account }}/usage-api:$AGAVE_VERSION
  volumes:
    - '/var/log/splunk/usage:/var/log/apache2'
    - './scratch/usage:/scratch'

tenants:
  extends:
    file: common.yml
    service: php_api
  image: {{ core_docker_registry_account }}/tenants-api:$AGAVE_VERSION
  volumes:
    - '/var/log/splunk/tenants:/var/log/apache2'
    - './ssl:/ssl:ro'
    - './scratch/tenants:/scratch'

logging:
  extends:
    file: common.yml
    service: php_api
  image: {{ core_docker_registry_account }}/logging-api:$AGAVE_VERSION
  volumes:
    - '/var/log/splunk/logging:/var/log/apache2'
    - './scratch/logging:/scratch'

docs:
  extends:
    file: common.yml
    service: php_api
  image: {{ core_docker_registry_account }}/apidocs-api:$AGAVE_VERSION
  volumes:
    - '/var/log/splunk/docs:/var/log/apache2'
    - './scratch/docs:/scratch'

{% if core_deploy_stats %}
stats:
  extends:
    file: common.yml
    service: php_api
  image: {{ core_docker_registry_account }}/stats-api:$AGAVE_VERSION
  environment:
    - 'PINGDOM_USERNAME={{ core_pingdom_username }}'
    - 'PINGDOM_PASSWORD={{ core_pingdom_password }}'
    - 'MYSQL_HOST={{ mysql_core_host }}'
    - 'PINGDOM_TOKEN={{ core_pingdom_token }}'
    - 'STATUSIO_ID={{ core_statusio_id }}'
    - 'FREEGEOIP_URL={{ core_freegeoip_url }}'
    - 'STATS_CACHE_LIFETIME=240'
    - 'DOCUMENT_ROOT=/var/www/html/public'
    - 'REDIS_HOST={{ core_redis_host }}'
    - 'REDIS_PORT={{ core_redis_port }}'
    - 'APIM_MYSQL_HOST={{ auth_mysql_host }}
    - 'APIM_MYSQL_PORT={{ auth_mysql_port }}'
    - 'APIM_MYSQL_USERNAME={{ core_auth_mysql_username }}'
    - 'APIM_MYSQL_PASSWORD={{ core_auth_mysql_password }}'
    - 'IPLANTC_ORG_MYSQL_HOST={{ core_iplant_mysql_host }}'
    - 'IPLANTC_ORG_MYSQL_PORT={{ core_iplant_mysql_port }}'
    - 'IPLANTC_ORG_MYSQL_USERNAME={{ core_iplant_mysql_username }}'
    - 'IPLANTC_ORG_MYSQL_PASSWORD={{ core_iplant_mysql_password }}'
    - 'IPLANTC_ORG_MYSQL_DATABASE={{ core_iplant_mysql_database }}'
    - 'ARAPORT_ORG_MYSQL_HOST={{ core_aip_mysql_host }}'
    - 'ARAPORT_ORG_MYSQL_PORT={{ core_aip_mysql_port }}'
    - 'ARAPORT_ORG_MYSQL_USERNAME={{ core_aip_mysql_username }}'
    - 'ARAPORT_ORG_MYSQL_PASSWORD={{ core_aip_mysql_password }}'
    - 'ARAPORT_ORG_MYSQL_DATABASE={{ core_aip_mysql_database }}'
  volumes:
    - '/var/log/splunk/stats:/var/www/html'
    - './scratch/stats:/scratch'


###############################################################
# Third-party Services
#
# These are the third-party services providing persistence and
# messaging to the Core Science APIs. The containers defined
# below are are sufficient for running in a development
# environment. Do NOT use these for production deployments.
#
# Production deployments should leverage a dedicated MySQL
# cluster, a managed MongoDB cluster, and a distributed
# message queue. Each of these should be operated in a high
# availability configuration. Without such a deployment, the
# Core Science APIs cannot be properly scaled to provide HA.
###############################################################

reverseip:
  image: fiorix/freegeoip
  ports:
    - "8080"

redis:
  image: redis:alpine
  command: redis-server --appendonly yes
  hostname: docker.example.com
  privileged: true
  ports:
    - '6379:6379'
  volumes:
    - ./cache/redis:/data

{% endif %}