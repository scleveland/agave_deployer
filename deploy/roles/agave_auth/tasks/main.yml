# Installs/updates the auth infrastructure containers using docker compose.

---

- include_vars: tenants/{{ tenant_id }}/{{ tenant_id }}.yml

- set_fact:
    api_server_base: "{{ host }}"
  when: api_server_base is undefined

- debug: msg="{{ api_server_base }}"

- name: ensure directories are present
  file: path={{ item }} state=directory
  sudo: yes
  sudo_user: apim
  with_items:
      - /home/apim/A
      - /home/apim/B
      - /home/apim/logs
      - /home/apim/{{ tenant_id }}
      - /home/apim/{{ tenant_id }}/docs/v2

- name: create logs file
  file: path={{ item }} state=touch
  sudo: yes
  sudo_user: apim
  with_items:
      - /home/apim/logs/apim-logs
      - /home/apim/logs/httpd-access-logs
      - /home/apim/logs/httpd-error-logs

- name: copy the tenant config file
  copy: src=tenants/{{ tenant_id }}/{{ tenant_id }}.yml dest=/home/apim/{{ tenant_id }}/{{ tenant_id }}.yml
  sudo: yes
  sudo_user: apim

- name: copy the password file
  copy: src=tenants/{{ tenant_id }}/{{ tenant_id }}_passwords dest=/home/apim/{{ tenant_id }}/passwords
  sudo: yes
  sudo_user: apim

- name: copy tenant-specific httpd files
  copy: src=tenants/{{ tenant_id }}/httpd/ dest=/home/apim/{{ tenant_id }}/httpd
  sudo: yes
  sudo_user: apim

- name: register custom_docs_exist
  local_action: stat path="{{ playbook_dir }}/tenants/{{ tenant_id }}/docs/v2/"
  register: custom_docs_exist

- name: copy custom docs
  copy: src=tenants/{{ tenant_id }}/docs/v2 dest=/home/apim/{{ tenant_id }}/docs/
  when: custom_docs_exist.stat.exists == True
  sudo: yes
  sudo_user: apim

- name: download the swagger docs template
  get_url:
      url: https://raw.githubusercontent.com/TACC/agavepy/master/agavepy/resources.json.j2
      dest: "{{ playbook_dir }}/roles/agave_auth/templates/agaveapi_v2.json.j2"
  delegate_to: 127.0.0.1
  when: custom_docs_exist.stat.exists == False

- name: compile the swagger template
  template: src=agaveapi_v2.json.j2 dest=/home/apim/{{ tenant_id }}/docs/v2/agaveapi.json
  sudo: yes
  sudo_user: apim
  when: custom_docs_exist.stat.exists == False

- name: copy tenant-specific apis
  copy: src=tenants/{{ tenant_id }}/apis/ dest=/home/apim/{{ tenant_id }}/apis
  sudo: yes
  sudo_user: apim

- name: copy custom oauth app
  copy: src=tenants/{{ tenant_id }}/authenticationendpoint/ dest=/home/apim/{{ tenant_id }}/authenticationendpoint
  sudo: yes
  sudo_user: apim
  when: update_custom_oauth_app

- name: copy default oauth app
  copy: src=authenticationendpoint/ dest=/home/apim/{{ tenant_id }}/authenticationendpoint
  sudo: yes
  sudo_user: apim
  when: update_default_oauth_app

# first, compile the compose file for the "green" deployment
- set_fact:
    primary_auth_compose: True

- name: Compile docker_compose template - A
  template: src=docker-compose-noserf.yml.j2 dest=/home/apim/A/docker-compose-noserf.yml
  sudo: yes
  sudo_user: apim

# now, compile the compose file for the "blue" deployment
- set_fact:
    primary_auth_compose: False

- name: Compile docker_compose template - B
  template: src=docker-compose-noserf.yml.j2 dest=/home/apim/B/docker-compose-noserf.yml
  sudo: yes
  sudo_user: apim

- name: Compile hap template
  template: src=hap-compose.yml.j2 dest=/home/apim/hap-compose.yml
  sudo: yes
  sudo_user: apim

- name: ensure haproxy is running
  shell: docker-compose -f /home/apim/hap-compose.yml up -d
  sudo: yes
  sudo_user: apim

- name: Compile pull_images template
  template: src=pull_images.sh.j2 dest=/home/apim/pull_images.sh
  sudo: yes
  sudo_user: apim

- name: set permissions on pull_images.sh
  acl: permissions="rwx" name=/home/apim/pull_images.sh state=present etype=user entity=apim
  sudo: yes

- name: Pull latest docker images
  shell: sh pull_images.sh chdir=/home/apim
  sudo: yes
  sudo_user: apim