# Run this playbook to deploy a new tenant to either staging or production, creating the necessary databases in the
# database server.
#
# NOTE: This playbook requires Ansible v2.0+
#
# The parameters for this playbook are:
# 1. tenant_id: used to determine which Docker images to deploy.
# 2. hosts file: should be either 'prod_hosts' or 'staging_hosts' (passed using -i)
# 3. env: the Agave environment (either 'staging' or 'prod', all lower case). This selects the correct group_vars to use.
# 4. mysql_user and mysql_password: credentials used to authenticate to the mysql server. This account should have admin
#    privileges on the entire MySQL instance: in particular, it should have the necessary privileges to create databases
#    and update user privileges. It is NOT the set of credentials used by the tenant - those are configured in the
#    passwords file.
# 5. mysql_login_host & mysql_login_port (OPTIONAL) - set these if the root user doesn't have direct access to mysql
#    *NOTE: these need to be set for staging since root does not have access via maxscale.
# Note: SSH credentials (e.g. ansible_ssh_host, ansible_ssh_user, ansible_ssh_private_key_file) should be placed in the
#       hosts file.
#
# Example invocation:
# $ ansible-playbook -i host_files/staging_hosts new_tenant.plbk -e env=staging -e tenant_id=dev_staging -e mysql_user=root -e mysql_password=<get_from_stache> -e mysql_login_host=172.17.42.1 -e mysql_login_port=3307

---

  # update database server first so that apim container can start correctly
- hosts: db
  vars:
      - ldap_host_ip: "{{ hostvars[groups['db'][0]]['ansible_eth0']['ipv4']['address'] }}"
      - auth_mysql_host: "{{ hostvars[groups['db'][0]]['ansible_ssh_host'] }}"
      - auth_beanstalk_server: "{{ hostvars[groups['db'][0]]['ansible_ssh_host'] }}"
      - core_host: "{{ hostvars[groups['core'][0]]['ansible_ssh_host'] }}"
  roles:
    - mysql_apim

  # deploy and start containers
- hosts: auth
  vars:
      - ldap_host_ip: "{{ hostvars[groups['db'][0]]['ansible_eth0']['ipv4']['address'] }}"
      - auth_mysql_host: "{{ hostvars[groups['db'][0]]['ansible_ssh_host'] }}"
      - auth_beanstalk_server: "{{ hostvars[groups['db'][0]]['ansible_ssh_host'] }}"
      - core_host: "{{ hostvars[groups['core'][0]]['ansible_ssh_host'] }}"
  roles:
    - agave_host
    - docker_host
    - agave_auth
    - ldap_apim