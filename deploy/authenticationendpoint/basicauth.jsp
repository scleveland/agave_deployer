<%@page import="org.wso2.carbon.identity.application.authentication.endpoint.util.CharacterEncoder"%>
<div id="loginTable1" class="identity-box">
    <%
        loginFailed = CharacterEncoder.getSafeText(request.getParameter("loginFailed"));
        if (loginFailed != null) {

    %>
    <div class="alert alert-danger">
                <fmt:message key='<%=CharacterEncoder.getSafeText(request.getParameter
                ("errorMessage"))%>'/>
            </div>
    <% } %>

    <% if (CharacterEncoder.getSafeText(request.getParameter("username")) == null || "".equals
    (CharacterEncoder.getSafeText(request.getParameter("username")).trim())) { %>

        <!-- Username -->
        <div class="form-group">
            <label class="control-label sr-only" for="username" ><fmt:message key='username'/>:</label>

            <div class="controls col-md-8">
                <input class="form-control" type="text" id='username' name="username" label="User Name" placeholder="Username" size='30'/>
            </div>
        </div>

    <%} else { %>

        <input type="hidden" id='username' name='username' value='<%=CharacterEncoder.getSafeText
        (request.getParameter("username"))%>'/>

    <% } %>

    <!--Password-->
    <div class="form-group">
        <label class="control-label sr-only" for="password"><fmt:message key='password'/>:</label>

        <div class="controls col-md-8">
            <input type="password" id='password' name="password"  class="form-control" label="password" size='30' placeholder="Password"/>
            <input type="hidden" name="sessionDataKey" value='<%=CharacterEncoder.getSafeText(request.getParameter("sessionDataKey"))%>'/>
            <label class="checkbox" style="margin-top:10px"><input type="checkbox" label="checkbox" id="chkRemember" name="chkRemember"><fmt:message key='remember.me'/></label>
        </div>
    </div>

    <div class="submit">
        <input type="submit" value='<fmt:message key='login'/>' class="btn btn-danger btn-block">
    </div>

</div>
<br />

<div class="help-link help-block" id="helpBlock"><span class="forgot-password"><a href="https://www.designsaf
e-ci.org/account/password-reset" target="_blank">Forgot Password</a></span> | <a href="https://agaveapi.atlassian.net/se
rvicedesk/customer/portal/2" title="Agave Support through Atlassian" target="_blank">Log In Help</a> | <a href="htt
p://status.agaveapi.co" target="_blank" title="Platform Status">Platform Status</div>
    </div>  
