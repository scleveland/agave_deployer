# Image: jstubbs/agave_deployer

from alpine:3.2

# packages
RUN apk add --update musl python && rm /var/cache/apk/*
RUN apk add --update musl python-dev && rm /var/cache/apk/*
RUN apk add --update py-pip && rm /var/cache/apk/*
RUN apk add --update bash && rm -f /var/cache/apk/*
RUN apk add --update git && rm -f /var/cache/apk/*
RUN apk add --update g++ -f /var/cache/apk/*
RUN apk add --update openssh -f /var/cache/apk/*
RUN apk add --update perl -f /var/cache/apk/*

# dependencies for ansible
RUN pip install paramiko PyYAML Jinja2 httplib2 six

# ansible source
ADD ansible /ansible

# docker_atim
ADD deploy /deploy

# entry
ADD entry.sh /entry.sh
RUN chmod +x /entry.sh

ENTRYPOINT ["./entry.sh"]
