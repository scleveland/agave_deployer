#!/bin/bash

./template_compiler/template_entry.sh

# append the container hosts file with the content created by ansible:
cat /tmp/hosts >> /etc/hosts

# start apim service:
cmd=$(/etc/init.d/apim start $*)
echo "$cmd"
#/etc/init.d/apim start

sleep infinity